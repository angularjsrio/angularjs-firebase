/**
 * Created by fabio on 20/04/16.
 */
var app = angular.module('mainModule',["firebase"]);

app.controller('mainController',function ($scope,$http,$firebaseAuth,Pessoas) {
    var ref = new Firebase("https://angularjsfb.firebaseio.com");

    var authData = ref.getAuth();
    if (authData) {
        console.log("User " + authData.uid + " is logged in with " + authData.provider);
    } else {
        console.log("User is logged out");
        ref.authWithOAuthPopup("facebook", function(error, authData) {
            if (error) {
                console.log("Login Failed!", error);
            } else {
                // the access token will allow us to make Open Graph API calls
                window.location = '';
            }
        }, {
            scope: "email,user_friends,user_photos" // the permissions requested
        });
    }

    $scope.imagens = [];

    $http.get('https://graph.facebook.com/v2.1/me/photos?fields=picture&access_token='+authData.facebook.accessToken)
        .then(function(res) {
            $scope.imagens = res.data.data;
    });

    $scope.tempimagefilepath = authData.facebook.profileImageURL;
    $scope.lista = Pessoas;

    $scope.salvar = function () {
        Pessoas.$add($scope.pessoa);
        $scope.pessoa = null;
    }

    $scope.remove = function (pessoa) {
        var index = $scope.lista.indexOf(pessoa);
        $scope.lista.splice(index,1);
    }

    $scope.editar = function (pessoa) {
        $scope.pessoa = pessoa;
    }

});
app.factory("Pessoas", function($firebaseArray) {
    var itemsRef = new Firebase("https://angularjsfb.firebaseio.com/pessoas");
    return $firebaseArray(itemsRef);
});


